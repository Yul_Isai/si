from cryptography.fernet import Fernet
 
class EncryptFiles():
    def keyEncrypt(self):
        key = Fernet.generate_key
        return key

    def KeyWrite(self, key, key_name):
        with open(key_name, 'wb') as filekey:
            filekey.write(key)

    def key_load(self, key_name):
        with open(key_name, 'rb') as mykey:
            key = mykey.read()
        return key


    def fileEncrypt (self, key, original_file, encrypted_file):
        fk = Fernet(key)

        with open(original_file, 'rb') as fileEnc:
            original = fileEnc.read()
        
        encrypted = fk.encrypt(original)

        with open(encrypted_file, 'wb') as file:
            file.write(encrypted)

    def fileDecrypt (self, key, encryptedFile, decryptedFile):
        fk = Fernet(key)

        with open(encryptedFile, 'rb') as fileEnc:
            encrypted = fileEnc.read()

        decrypted = fk.decrypt(encrypted)

        with open(decryptedFile, 'wb') as file:
            file.write(decrypted)




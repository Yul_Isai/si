from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Exercises(models.Model):
    original_code = models.CharField(max_length=255)
    hash_code = models.TextField(max_length=80)
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.original_code + ' by - ' + self.user.username

class Document(models.Model):
    title = models.CharField(max_length=200)
    uploadedFile = models.FileField(upload_to = "Uploaded_Files/")
    dateTimeOfUpload = models.DateTimeField(auto_now = True)
from dataclasses import fields
from django.forms import ModelForm
from .models import Exercises
from .models import Document
class ExceriseForm(ModelForm):
    class Meta:
        model = Exercises
        fields = ['original_code']

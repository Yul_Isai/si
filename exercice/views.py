from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.db import IntegrityError
from .forms import ExceriseForm
from .EncryptFiles import EncryptFiles
from .models import Exercises
from . import models
import hashlib
from django.contrib.auth.decorators import login_required
# Create your views here.


def home(request):
    username = User.objects.all()
    print(username)
    return render(request, 'template.html', {'username': username})


def signup(request):
    if request.method == 'GET':
        return render(request, 'signup.html', {
            'form': UserCreationForm,
        })
    else:
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.create_user(
                    username=request.POST['username'], password=request.POST['password1'])
                user.save()
                login(request, user)
                return redirect('exercise')
            except IntegrityError:
                return render(request, 'signup.html', {
                    'form': UserCreationForm,
                    'error': 'Username already exists'
                })
        return render(request, 'signup.html', {
            'form': UserCreationForm,
            'error': 'Password dont match'
        })

@login_required
def view_exercice(request):
    exercises = Exercises.objects.filter(
        user=request.user, hash_code__isnull=False)
    return render(request, 'hash_view.html',
                  {
                      'exercises': exercises
                  })

@login_required
def log_out(request):
    logout(request)
    return redirect('home')


def log_in(request):
    if request.method == 'GET':
        return render(request, 'login.html', {
            'form': AuthenticationForm
        })
    else:
        user = authenticate(
            request, username=request.POST['username'], password=request.POST['password'])

        if user is None:
            return render(request, 'login.html', {
                'form': AuthenticationForm,
                'state': False,
                'error': 'Upss!! \n username or password is incorrect'
            })
        else:
            login(request, user)
            return redirect('exercise')

@login_required
def new_exercise(request):

    if request.method == 'GET':
        return render(request, 'new_exercise.html', {
            'form': ExceriseForm
        })
    else:
        try:
            print(request.POST)
            form = ExceriseForm(request.POST)
            uid = encrypt_code_SHA256(request.POST['original_code'])
            new_exercise = form.save(commit=False)
            new_exercise.user = request.user
            new_exercise.hash_code = uid
            new_exercise.save()
            return redirect('exercise')
        except ValueError:
            return render(request, 'new_exercise.html', {
                'form': ExceriseForm,
                'error': 'Ingresa un codigo valido'
            })

@login_required
def Exec_detail(request, ex_id):
    if request.method == 'GET':
        exec = get_object_or_404(Exercises, pk=ex_id)
        form = ExceriseForm(instance=exec)
        return render(request, 'exercise_detail.html',
                    {
                        'exercise': exec,
                        'form': form
                    })
    else:
        try:
            exec = get_object_or_404(Exercises, pk=ex_id)
            form = ExceriseForm(request.POST, instance=exec)
            uid = encrypt_code_SHA256(request.POST['original_code'])
            update_exercise = form.save(commit=False)
            update_exercise.hash_code = uid
            update_exercise.save()
            return redirect('exercise')
        except ValueError:
            return render(request, 'exercise_detail.html',
                    {
                        'exercise': exec,
                        'form': form,
                        'error': 'No se puede actualizar'
                    })

def encrypt_code_SHA256(code):
    if not isinstance(code, bytes):
        code = str(code).encode()
    return hashlib.sha256(code).hexdigest()

@login_required
def uploadFiles(request):
    if request.method == 'POST':
        fileTitle = request.POST["fileTitle"]
        uploadedFile = request.FILES["uploadedFile"]
        document = models.Document(
            title = fileTitle,
            uploadedFile = uploadedFile
        )
        document.save()
        
    documents = models.Document.objects.all()

    return render(request, "upload.html", context = {
        "files": documents
    })

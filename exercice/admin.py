from django.contrib import admin
from .models import Exercises
# Register your models here.

class ExcAdmin(admin.ModelAdmin):
    readonly_fields = ("created", )

admin.site.register(Exercises, ExcAdmin)